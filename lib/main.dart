import 'package:flutter/material.dart';
import 'package:sqflite_example/ui/listview_note.dart';

void main() => runApp(
  MaterialApp(
    title: 'Returning data',
    home: ListViewNote(),
  )
);
